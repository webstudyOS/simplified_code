
// this is the code which will be injected into a given page

var isActive = true;
console.log("FROM NEW JS INJECT");


window.onfocus = function () {
  isActive = true;
  //Starts tab timer again IF it is not a repeat call that 
  //has already been covered by a tab listener
  tabMessage(true);
};


window.onblur = function () {
  isActive = false;

  tabMessage(false);
};

//Sends a message to background to either start or stop the timer for this tab based 
// on whether the flag is true or false, respectively
function tabMessage(flag) {
  chrome.runtime.sendMessage({start: flag});
}


//This interval is just for testing
setInterval(function () {
  console.log(window.isActive ? 'active' : 'inactive');
}, 2000);


