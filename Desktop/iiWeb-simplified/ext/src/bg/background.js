console.log("Hello, world! test");

//TEST query for audible tabs
chrome.tabs.query({windowType: 'normal', audible: true},  
    function(activeTabs) { //callback
      console.log("Initial ActiveTAB query: tab#" +  
        (activeTabs[0] ? activeTabs[0].id : "NO TAB") +
        "\nlength- " + activeTabs.length + "  " + JSON.stringify(activeTabs));
    });



//used to save times when a bad shutdown or sleepmode occurs)
var lastIntervalTime = 0; //default (needed?)

//***** FIRST INSTALL of chrome extension **********************************************

chrome.runtime.onInstalled.addListener(function(details){ 
  //clear storage area (TODO:Test this on diff users. Does this clear everything or just for current user?)
  chrome.storage.sync.clear(); 
  console.log("Installed and Cleared " + JSON.stringify(details));
  
  lastIntervalTime = (new Date()).getTime();
});
//******* END INSTALL ***************************************************************




//=========DEFINE GLOBAL VARIABLES for background file ============

var alarmName = "alarmStage"

var DEFAULTTIME = 1000 * 60 * 2; //2min---TODO:correct Time SHOULD be 1000*60*60*24 == millisecs in 24 hrs 
var CHROME_OUT_OF_FOCUS = -1; //constant val

// null data will be updated in the browseraction once user inputs information
var userID = null;

var userMetaData = {usertype: null, websites: null}; //ex. starting data {type: 2, websites: ['facebook', 'netflix']}

var webProgress = {}; //NOTE: the basic stucture is set wehen websites are chosen! 
var finalWebData = {};
//Used to keep track of durations of tabs with distract wesites based on tab-id
var tabsSeen = {}; //internal structure-- tabId: {website: url, starttime: time}
var WEB_TOTAL_IDX = 1;

var stage = 0; //starts at 0 until first 24 hours (1) then 2nd 24 hours (2) 
var lastStage = 1; // is 1 for simplified version


//Indicates what url the browser action should show when clicked
var curBrowserActionPage = {page: 'src/browser_action/browser_action.html', websites: null}; 

var timer1 = 0;
var timer2 = 0;
var intervalTime = 15; //in seconds

//save this global var
var prevTimers = {activeId: CHROME_OUT_OF_FOCUS};


// Initialize Firebase (could not be put in the html file!)
var config = {
  apiKey: "AIzaSyB_UYQNGC-jG9YqHDff9xcqqyIF53iApwc",
  authDomain: "iiweb-database.firebaseapp.com",
  databaseURL: "https://iiweb-database.firebaseio.com",
  storageBucket: "iiweb-database.appspot.com",
};

firebase.initializeApp(config);

//Get instance of Firebase database
var database = firebase.database();

//======= END GLOBAL VARIABLES ============





//********** CHROME MESSAGES THAT START TIMERS *******************************

 
chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
    console.log("**inside ONmessage!: ", message);

    //RECIEVES MESSAGE FROM BROWSER ACTION THAT STARTS THE TIMER**
    if (message.timeron){
      sendResponse('timer turned on in message');
      console.log("inside message timer on");

      stage = 1; //start of stage 1 
      
      //TURNS ON WEBSITE MONITORING for the first 24 hr timer
      webwatch(true, 1, true);
      getTimes(); //calculate endtimes for timers

      //starts first 24 hrs and web-monitoring
      starttimer();


    //test--RECIEVES MESSAGE FROM INJECTED TRACKFOCUS.JS CODE
    } else if (message.start && message.start == true) {
      sendResponse('started tabtimer in Message');

      //Starts tab timer again IF it is not a repeat call (has not been covered by the tab listener process yet)
      console.log("***message ACTIVATE tab# " + sender.tab.id + " from INJECTED");
      if (sender.tab) { //check if tab exists bc it is an 'optonal' feature 
        startOrIgnoreTab(sender.tab.id, sender.tab.url);
      } else {
        console.log("**ERROR, message from content script has no 'tab' object");
      }


    } else if (message.start && message.start == false) {
      sendResponse('stopped tabtimer in Message');

      //stops tab timer
      console.log("***message INACTIVATE tab# " + sender.tab.id + " from INJECTED");
      if (sender.tab) { //check if tab exists bc it is an 'optonal' feature 
        stopTabTimer(sender.tab.id);
      } else {
        console.log("**ERROR, message from content script has no 'tab' object");
      }


    } else {
      sendResponse("MESSAGE ERROR: message not correct for a timer");
    }
    return;
});

//********** END CHROME MESSAGES ***********************************




//BROWSER ACTION CLICK LISTENER -- will bring up a tab with login and info
chrome.browserAction.onClicked.addListener(function(tab) {
  console.log('browser action clicked');

  var mypage;
  //create url for page IF it is not already a real website url
  if (! curBrowserActionPage.page.match("http") ){
    mypage = chrome.extension.getURL(curBrowserActionPage.page);
  } else {
    mypage = curBrowserActionPage.page;
  }

  //opens new tab if the page is not already open
  chrome.tabs.query({url: mypage},  
    function(myTabs) {
      if (myTabs.length > 0) {
        //make old tab active
        console.log('TRYING TO make old browser Tab active!!');

        //make window focused
        chrome.windows.update(myTabs[0].windowId, {"focused": true} );

        if (myTabs[0].active == false) {
          //make tab active in that window
          chrome.tabs.update(myTabs[0].id,{"active":true,"highlighted":true},function (tab){
            console.log("made old browser Tab active!!  " + tab.id);
          });
        }

      } else {
        //creates new tab if none exists
        chrome.tabs.create( {'url': mypage},
            function(tab) { //Callback gives me the'tab' obj that was just created
            // Tab opened.
              console.log('opened new browser Tab!!');
            }
        );
      }
    });
});





//=============== TAB LISTENERS (callback methods at bottom of this file)===================================================================================

// Starts tab timer on whatever tab is currently active
function getInitialActiveTab() {

  chrome.tabs.query({active: true, lastFocusedWindow: true, windowType: 'normal'},  
    function(activeTabs) { //callback
      console.log("Initial ActiveTAB query: tab#" +  (activeTabs[0] ? activeTabs[0].id : "NO TAB") 
        + "\nlength- " + activeTabs.length + "  " + JSON.stringify(activeTabs));

      if (activeTabs.length > 0) {
        //save tabId for future use
        prevTimers.activeId = activeTabs[0].id;

        startOrIgnoreTab(activeTabs[0].id, activeTabs[0].url); 
      }
    });
}

function startTabListeners(){
  console.log("*****IN START_TAB_LISTENERS****");
  chrome.tabs.onUpdated.addListener(onTabUpdated);

  chrome.tabs.onActivated.addListener(onTabActivated); 

  chrome.windows.onFocusChanged.addListener(onWindowFocusChanged);

  chrome.tabs.onRemoved.addListener(onTabRemoved);

  chrome.tabs.onReplaced.addListener(onTabReplaced); //TODO: test more with facebook.com
}

//Remove all Listeners
function stopTabListeners(){
  console.log("*****IN STOP_TAB_LISTENERS****");
  chrome.tabs.onUpdated.removeListener(onTabUpdated);
  chrome.windows.onFocusChanged.removeListener(onWindowFocusChanged);
  chrome.tabs.onActivated.removeListener(onTabActivated); 
  chrome.tabs.onRemoved.removeListener(onTabRemoved);
  chrome.tabs.onReplaced.removeListener(onTabReplaced);
}
//============== END TAB LISTENERS ===================================================================================




//=============== WEB MONITORING METHODS ===============================================
//========================================================================================

//Checked whether a given url is one of the distract websites.
//Either returns the name of the website or null
function isBlocked(tabUrl)
{
  //check that neither is null
  if (userMetaData && userMetaData.websites) { 
    for (var i = 0; i < userMetaData.websites.length; i++){
      var b = userMetaData.websites[i]; //blacklist[item];
      if(tabUrl.match(b) != null && tabUrl.match(chrome.extension.getURL("")) == null)
      {
        return b;
      }
    }
  }
  return null;
}


//Calculates and saves the times when stage 1 and 2 should end
function getTimes() {
  // Saves expected stop times for current timer and future timer
  var curTime = new Date().getTime();
  timer1= curTime + DEFAULTTIME;
  timer2= curTime + DEFAULTTIME * 2;
}

//This listens for the completion of alarms started below in starttimer()
chrome.alarms.onAlarm.addListener(function( alarm ) {
  console.log("Got an ALARM when it finished!! ", alarm);
  //alert("Got an ALARM when it finished!! " + JSON.stringify(alarm));

  switch (alarm.name) {
    case (alarmName + 1):
      afterFirstTimer();
      break;

    default:
      console.log("some other alarm");
      break;
  }
});


//starts the Alarm timer for the FIRST 24 hr period (stage 1)
function starttimer(){
  chrome.alarms.create(alarmName + 1, {when: timer1});
 }


//Completes tasks after the first 24 hr timer for stage 1
function afterFirstTimer() { 
  //ends monitoring for stage 1
  webwatch(false, 1, false);
  //alert("WebTime SO FAR" + JSON.stringify(webProgress));

  //Change to stage 2 but code for it has been removed for this simplified version 
  stage = 2;
}


//Saves time on websites as they occur
function localSaveWebTime(website, duration){
  
  webProgress[stage][website].push(duration); //adds the new duration period to the given website
  if (duration >= 0){ //Does not add negative error numbers into the total!
    webProgress[stage][website][WEB_TOTAL_IDX] += duration; //updates the total time on website
  }
}


//Goes through tabsSeen to stop any timers that are left over for the current stage
function saveLeftOverTimes(){
  console.log("**inside saveLeftOverTimes()");
  for (var seen in tabsSeen) {
    //TODO--test if this works!! Does it check if seen is null??
    if (tabsSeen.hasOwnProperty(seen) && tabsSeen[seen]){
      console.log("**stopping tab " + seen + " " + tabsSeen[seen]);
      stopTabTimer(seen);
    }
  }
}


//Does the final set up of the object to be uploaded to the database
function databaseSaveWebTime(){

  //saves any unsaved distract times 
  saveLeftOverTimes(); 

  var total = 0;
  var tempDblArr = [];
  var webArray = userMetaData.websites;
  var curweb;
  if (webArray) { //check for null
    for (var i = 0; i < webArray.length; i++) {
      //Handle case for an empty webProgress var (which happens if no distract sessions 
      //occured before a shutdown)
      if (webProgress[stage] && webProgress[stage][webArray[i]]){
        curweb = webProgress[stage][webArray[i]];
        total += curweb[WEB_TOTAL_IDX];
      } else {
        curweb = [webArray[i], 0]; //note that this website had 0 total distract time
        console.log("**ERROR: problem with webProgress, so had to use a default 0");
      }
      tempDblArr.push(curweb); //makes double array of all the website durations
    }
  } else {
    console.log("**ERROR: userMetaData.websites is NULL in databaseSaveWebTime");
  }
  finalWebData["websites" + stage] = tempDblArr;
  finalWebData["time" + stage] = total;
  console.log("FINAL web data: " + JSON.stringify(finalWebData));

  //upload finalWebData to database at the end of last stage
  if (stage == lastStage){
    database.ref('duration/' + userID).set(finalWebData);
    console.log("**sent to Database");
    alert("Sent to Database at end of stage " + lastStage);
  }
}


// WEBSITE MONITORING Function. if flag is true, starts tab timers for that stage
//if flag is false, it ends all of the tab timers for that stage and records them
function webwatch(flag, stage, isNewTimer){
  var datetimeStr;
  var stageStr;

  if (flag == true) {

    if (isNewTimer) {
      //first, get and save date/time
      datetime = new Date();
      datetimeStr = "" + datetime;
      stageStr = "start" + stage;
      //uploads date and time marker into the database as the start point of the given stage
      //saveTimeMarker(userID, stageStr, datetimeStr); //UNCOMMENT
    }

    //starts web monitoring
    getInitialActiveTab();
    startTabListeners();
    
  } else { //if flag is false, turns off web monitor 
    
    console.log("*****IN WEBWATCH 'false'****** stage: " + stage);
    //Saves the web times for the previous stage in the final format(which is uploaded to database after stage 2)
    databaseSaveWebTime();

    //stops the web monitoring if at end of last stage;
    if (stage == lastStage){
      console.log("Ending all web monitoring at end of stage " + stage);
      stopTabListeners();
    }

    //first, get and save date/time
    datetime = new Date();
    datetimeStr = "" + datetime;
 
    stageStr = "end" + stage;
    //saves the date and time that the stage ENDS 
    //saveTimeMarker(userID, stageStr, datetimeStr); //UNCOMMENT
  }
}



/*Given a tab and past data, this decides whether to call startTabTimer, stopTabTimer,
or do nothing as needed.*/
function startOrIgnoreTab(tabId, curTabUrl){

  console.log("startOrIgnoreTab(): Url: " + curTabUrl + " ID: " 
    + tabId + " has " + tabsSeen[tabId] + " inTabsSeen");
  
  //Check that url is never null
  if (curTabUrl) { 

    var seen = tabsSeen[tabId]; //will return an object is tab previously had a distract session
    //saves the distract website name that the url matches with or Null if it's not a distract site
    var curblockedWeb = isBlocked(curTabUrl);

    if (curblockedWeb)//check for null
    {
      console.log("BLOCKED: " + curTabUrl + "  " + tabId + " " + curblockedWeb);

      if (seen) //check for non-null which indicates that there was already a distract session on this tab
      {
        //so this is a different distract session on same tab. End timer on previous session and starts new one
        if (seen.website != curblockedWeb)
        {
          console.log("previous distract");
          stopTabTimer(tabId);
          startTabTimer(tabId, curblockedWeb);
        }
      } else { //NEW distract session: startTabTimer saves the new website and starts the new time counter
        
        //tabsSeen[tabId] = curblockedWeb; //adds tabid->website because this tab has been used on distract site
        startTabTimer(tabId, curblockedWeb);
        console.log("NEW DISTRACT session added to tabsSeen: " + JSON.stringify(tabsSeen));
        
      }
    } else { //Current site is NOT a distract site

      //Tab was previously on a distract session, so this marked the end of it
      if (seen) 
      {
        console.log("previous distract ended");
        //Ends timer for distract session
        stopTabTimer(tabId);
      }
      //Does nothing if current site and previous sites on the tab were not distracting 
    }
  } 
    console.log("END startOrIgnoreTab()");
}



//Saves the time that the distract session on the current tab started
// AND shows a popup if in the 2nd stage
function startTabTimer(tabId, url)
{
  var d = new Date(); 
  var time = d.getTime();
  /*TEST inject focus tracking into distract page  
      TODO-- check that this script executes even when website updates or 
      goes to other pages but is still on same distract website... */
  chrome.tabs.executeScript({file: 'src/bg/trackFocus.js'});

  //creates new field in object tabsSeen and saves it
  tabsSeen[tabId] = {website: url, starttime: time};
}


function stopTabTimer(tabId)
{
  var tab = tabsSeen[tabId]
  console.log("tabStop called");

  //check that tab was on a distract session before (which now needs to be ended)
  if (tab) { 
    //deletes the object, thereby ending the timer on that distract session
    delete tabsSeen[tabId]; 

    var roundingOffset = 100; //used to round to 2 decimal places
    var endtime;

    //If tab was from a previous session, before a sleep or shutdown, then get the saved endtime.

    if (tabsSeen["retrievedEndTime"]) {
      console.log("**stopTabTimer FOR a RETRIEVED ENDTIME. time:" + tabsSeen["retrievedEndTime"] + " tab:" + tabId + "--" + tab);
      endtime = tabsSeen["retrievedEndTime"];

      //Delete "retrievedEndTime" from tabsSeen if there are no more previous tabs to stop
      if (Object.getOwnPropertyNames(tabsSeen).length === 1) {
        console.log("**deleted 'retrievedEndTime' property in tabsSeen");
        delete tabsSeen["retrievedEndTime"];
      }

    } else { //For current sessions, the endtime is the current time
      console.log("stopTabTimer FOR a CURRENT ENDTIME");
      var d = new Date();
      endtime = d.getTime();
    }

    //calculate duration of the previous distract session 
    endtime = (endtime - tab.starttime) / 1000 / 60; // change millisecs to minutes

    alert("END TAB TIMER" + tab.website + " time: " + endtime + "\n" + JSON.stringify(webProgress));
    //locally save duration (with time rounded to 2 decimal places)
    localSaveWebTime(tab.website, (Math.round(endtime * roundingOffset) / roundingOffset) );

  } else {
    console.log("NO tab STOP on " + tabId);
  }
}
//===============END OF  WEB MONITORING METHODS ==================================



//===============================================================================================================
//=================CALLBACK METHODS FOR TAB LISTENERS ========================================================================


function onWindowFocusChanged(windowId) {

  // Query for last focused active tab
  chrome.tabs.query({active: true, lastFocusedWindow: true, windowType: 'normal'},  
    function(activeTabs) { //callback

      console.log("WINDOWFOCUS: " + windowId + " PREV TAB= " + prevTimers.activeId);


      //is CHROME is OUT OF FOCUS
      if (activeTabs.length == 0) { 
        console.log("no active tabs found in onWindoeFocusChanged");

        //If Chrome was previously focused, then it stops the tabtimer 
        //[this check handles repeat calls] 
        if (prevTimers.activeId != CHROME_OUT_OF_FOCUS) {
          
          stopTabTimer(prevTimers.activeId);
          prevTimers.activeId = CHROME_OUT_OF_FOCUS; //set to impossible id -1 bc Chrome is now out of focus
        }

      } else { //Chrome still in focus

        
        console.log("WINFOCUS cont >>Initial ActiveTAB query: tab#" +  activeTabs[0].id 
          + "\nlength- " + activeTabs.length + "  " + JSON.stringify(activeTabs));

        
        if (prevTimers.activeId == CHROME_OUT_OF_FOCUS) {
          console.log("Previously UNFOCUSED");
        }

        // Only saves tab and start timer if TAB is diff from previous Tab (note prev tab will be -1 if was out of focus) 
        //[this can help handle repeat calls like when WindowFocus and TabActiviated calls at same time]
        if (prevTimers.activeId != activeTabs[0].id) {
          
          //save tabId for future use then start tab timer if needed
          prevTimers.activeId = activeTabs[0].id;
          //determines whether to start a new tab timer on this tab or do nothing
          startOrIgnoreTab(activeTabs[0].id, activeTabs[0].url);  
        }

      }
    }
  );
}


//(I assume that if the replaced tab was active, then the new tab 
  //is the new active one)
function onTabReplaced(addedTabId, removedTabId)
{
  console.log("" + addedTabId + " REPLACED " + removedTabId);

  //Stops timer and starts new one only if the replaced Tab 
  //was the current active tab

  if (prevTimers.activeId == removedTabId) {
    console.log("now I care about the REPLACE. Old prevTAb: " + 
      prevTimers.activeId +"New prevTab: "+ addedTabId);

    //save curr as prev Tab id
    prevTimers.activeId = addedTabId;

    //end the timer for distract session on replaced tab if there was a timer
    stopTabTimer(removedTabId);

    //check current tab for distract session
    chrome.tabs.get(addedTabId, function(tab) { 
      //determines whether to start a new tab timer on this tab or do nothing
      startOrIgnoreTab(addedTabId, tab.url);

    });
  }
}


/* NOTE: This method stops the tab timer on this tab if a timer exists BUT
  the previous TabId is not reset here. That will be handled in either 
  the onActivated or WindowFocusChanged calls that will be made at the same 
  time as a onRemoved call bc they would know the new tab id */
function onTabRemoved(tabId, removeInfo)
{
  console.log("onREMOVED tab# " + tabId + "PREV Tab= " + prevTimers.activeId);
  //end the timer for distract session if there was one on this tab
  stopTabTimer(tabId);
}


function onTabActivated(activeInfo)
{
  //safety check that tabId is not null
  if(activeInfo.tabId){
    console.log("ACTIVATION Old PrevTab= " + prevTimers.activeId + 
      " NEW PrevTab=" + activeInfo.tabId);

    stopTabTimer(prevTimers.activeId); //stops previous timer if there was one
    // Set prev active tab
    prevTimers.activeId = activeInfo.tabId;
    chrome.storage.sync.set({"prevTimers": JSON. stringify(prevTimers)}, 
            function() {
              console.log('STORAGE test save "prevTimers" in onTabActivated()');
          });

    chrome.tabs.get(activeInfo.tabId, function(tab) {
        if (tab && !chrome.runtime.lastError){ //check for null and if an error was defined
          //onTabUpdated(activeInfo.tabId, {status:"loading"}, tab); 
          startOrIgnoreTab(activeInfo.tabId, tab.url);
        } else {
          console.log("**ERROR: in onTabActivated, tab does not exist", chrome.runtime.lastError);
        }
    });
  }
}


//NOTE: tab is "updated" even when you press a link that takes you to the SAME page
function onTabUpdated(tabId,changeInfo,tab)
{
  if (changeInfo.status == "loading" && tab.url) //check that url exists already
  {
    //Checks tab if update was on an active tab.
    if (tabId == prevTimers.activeId) {
      console.log("UPDATED on *active* tab#" + tabId + ": " + tab.url + " (loading)");
      startOrIgnoreTab(tabId, tab.url); //function handles stop-tab-timer if needed
    }
  }
}

//============== END METHODS FOR TAB LISTENERS ====================================================
//================================================================================================


