//THIS FILE IS NOT USED


// linked in pop-up_draft.html file and background.html file

var default_options = {
  blocked_urls: [],
  enabled: true
};



// Storage helpers

function load_options(callback) {
  chrome.storage.sync.get(default_options, function(options) {
    callback(options); //WHERE DOES THE 'OPTIONS' VAR COME FROM??? DO I NEED TO DEFINE IT SOMEWHERE FIRST???? 
  });  
}

function store_options(options, callback) {
  chrome.storage.sync.set(options, function() {
    chrome.runtime.sendMessage(options);
    if(callback) {
      callback();
    }
  });
}

