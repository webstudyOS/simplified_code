
//This is the background javascript for browser_action that gets information from the 
//html text areas and gives the buttons functionality


var id_value = null;
var myType = 2; //default
var typeStrInBrowser = "default";
var wasInvalid = false;

// Jquery-- this function happens when the page loads.
// When Signin button is pushed, credentials are tested before going to next page
$(function () 
{
	$('#signin_IDbutton').click(function () {
		//check_signin returns a true boolean if the id is valid
		valid_id = check_signin();

		if (valid_id){ //check '&& validpass' IF using pin (uncomment)
			
			//save the correct 'current' page
			chrome.extension.getBackgroundPage().curBrowserActionPage.page = 'src/browser_action/browser_p2_urls.html';
			chrome.extension.getBackgroundPage().userMetaData.usertype = myType;
			
			//THEN go to next page to ask for websites
			window.location.href='browser_p2_urls.html';

		} else {
			alert("Invalid ID. Please recheck the email that was sent to you as a participant in this study");
			
			if (!wasInvalid){
				$("h1").after("<p><b>Please Try Again</b></p>");
				wasInvalid = true;
			}
		}
	});

});



function check_signin()
{
	
	id_value = ($('input[name="new_id"]').val()).trim(); //removes white space

	//validates id_value
	if (id_value != "" && id_value >= 0 && id_value < 1600 && (id_value%5==0)) {
		//save id
		chrome.extension.getBackgroundPage().userID = id_value;

		return true;
	} else {
		return false;
	}
}