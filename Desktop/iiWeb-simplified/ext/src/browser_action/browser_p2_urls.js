
//This is the background javascript for browser_action that gets information from 
//the html text areas and gives the buttons functionality

var min_websites = 1;



// Jquery-- this function happens when the page loads
$(function () 
{
	$('#savebutton').click(function () {
		save_urls();
	});
});


//checks that the min # of websites are given, then let them check that the 
//urls are correct
function save_urls()
{
    urls_value = $('#distract_urls').val();
    var urlsArray = urls_value.split('\n');

    //cleans up user entry
    for(i = 0; i < urlsArray.length; i++)
    {
        if(urlsArray[i] == '')
        {
			//removes any text input that were empty strings 
            urlsArray.splice(i,1);
            i--; //adjust index bc of removed elements
        } 
        //Checks for and deletes repeats
        else if (urlsArray.indexOf(urlsArray[i]) < i) {
			//removes the current text input that was a repeat 
            urlsArray.splice(i,1);
            i--; //adjust index bc of removed elements
        }   
    }
	
	//check that they gave enough websites
	if (urlsArray.length < min_websites) {
		alert("Please include at least " + min_websites +  " website");
		return; //leave function
	}

	//locally save website list 
	var bgMetaData = chrome.extension.getBackgroundPage().userMetaData;
	bgMetaData.websites = urlsArray;


	//finish setting up webProgress data structure in the background page 
	var progress = chrome.extension.getBackgroundPage().webProgress;
	var curWeb;
	
	for (var stageIdx = 1; stageIdx <= 2; stageIdx++) {
		progress[stageIdx] = {};
		for (var i = 0; i < urlsArray.length; i++) {
			curWeb = urlsArray[i];
			progress[stageIdx][curWeb] = [curWeb, 0];
		}
	}
	
	chrome.extension.getBackgroundPage().curBrowserActionPage.page = 'src/browser_action/browser_p3_starttime.html';

	//THEN go to finish page
	window.location.href='browser_p3_starttime.html';

	//********START TIMER
	// alerts Background to start timer
	chrome.runtime.sendMessage({timeron: true}, function(response) {
		console.log("TimerOn in sendMessage: ", response);
	});


}
