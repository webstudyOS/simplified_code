This is a somewhat simplified version of my actual Chrome extension. It has all of the tablisteners and webmonitoring 
that I currently use and it has a test query at the top to look for audible tabs. 
NOTE that I call often refer to 'distract tabs or websites'---those are just the websites tht are
inputted by the user in the browser action. 


About THE CODE
--All of the main code is in ext/src/bg/background.js
--the code in ext/bg/trackFocus.js is injected into distract tabs (by startTabTimer() in background.js) in my attempt to 
measure time on the page (clicking, etc) rather than time elsewhere while the website is open(clicking your 
computer desktop or other program). THEN the message is sent to line 104 in the background page.
It seems that I can't use runtime.message multiple times so I need a connection instead.


TO RUN THIS, go to the Chrome extensions page and drag and drop the 'ext' folder there to load it.
Then click the small, blue, world browser action icon and put in 55 as the id. On the next page,
I always put help.com as my test "distract website". The web monitoring will occur immediately after that page
(you can see the progress in he background page console.) Time on websites will only be tracked if it on a given distract website.